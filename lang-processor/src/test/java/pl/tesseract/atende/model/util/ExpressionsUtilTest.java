package pl.tesseract.atende.model.util;

import org.junit.Test;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Paweł Mielcarek
 */
public class ExpressionsUtilTest {

    @Test
    public void createSimpleExpressionTest() {
        String expression = "(a > 2) or (b <= -3)";
        List<String> expectedSimpleExpression = Arrays.asList("(", "a", ">", "2", ")", "or", "(", "b", "<=", "-3", ")");
        assertEquals(expectedSimpleExpression, ExpressionsUtil.createSimpleExpression(expression));
    }
}
