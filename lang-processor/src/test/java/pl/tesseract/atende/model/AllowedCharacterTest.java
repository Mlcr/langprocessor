package pl.tesseract.atende.model;

import org.junit.Test;

/**
 * @author Paweł Mielcarek
 */
public class AllowedCharacterTest {

    @Test
    public void findBySymbol() {
        assert AllowedCharacter.findBySymbol("a") == AllowedCharacter.A;
        assert AllowedCharacter.findBySymbol("b") == AllowedCharacter.B;
        assert AllowedCharacter.findBySymbol("c") == AllowedCharacter.C;
        assert AllowedCharacter.findBySymbol("d") == AllowedCharacter.D;
        assert AllowedCharacter.findBySymbol("e") == AllowedCharacter.E;
        assert AllowedCharacter.findBySymbol("f") == AllowedCharacter.F;
        assert AllowedCharacter.findBySymbol("g") == AllowedCharacter.G;
        assert AllowedCharacter.findBySymbol("h") == AllowedCharacter.H;
        assert AllowedCharacter.findBySymbol("(") == AllowedCharacter.LEFT_BRACKET;
        assert AllowedCharacter.findBySymbol(")") == AllowedCharacter.RIGHT_BRACKET;
        assert AllowedCharacter.findBySymbol("and") == AllowedCharacter.AND;
        assert AllowedCharacter.findBySymbol("or") == AllowedCharacter.OR;
        assert AllowedCharacter.findBySymbol("<") == AllowedCharacter.LESS;
        assert AllowedCharacter.findBySymbol("<=") == AllowedCharacter.LESS_EQUALS;
        assert AllowedCharacter.findBySymbol(">") == AllowedCharacter.GREATER;
        assert AllowedCharacter.findBySymbol(">=") == AllowedCharacter.GREATER_EQUALS;
        assert AllowedCharacter.findBySymbol("=") == AllowedCharacter.EQUALS;
    }

    @Test
    public void isAllowedCharacter() {
        for (AllowedCharacter allowedCharacter : AllowedCharacter.values()) {
            assert AllowedCharacter.isAllowedCharacter(allowedCharacter.getSymbol());
        }
    }

    @Test
    public void isInteger() {
        assert AllowedCharacter.isInteger("1");
        assert AllowedCharacter.isInteger("171");
        assert AllowedCharacter.isInteger("-1");
        assert AllowedCharacter.isInteger("-69");
    }
}
