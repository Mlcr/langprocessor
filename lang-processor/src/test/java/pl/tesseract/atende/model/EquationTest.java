package pl.tesseract.atende.model;

import org.junit.Test;

/**
 * @author Paweł Mielcarek
 */
public class EquationTest {

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalConstructorArgument() {
        new Equation("zcxa123'!!!");
    }

    @Test
    public void testLegalConstructorArguments() {
        new Equation("(a = 2) or (b >= -3)");
    }

    @Test
    public void testExpressionsTree() {
        ExpressionNode expectedRootNode = new ExpressionNode("or", 0);
        ExpressionNode node2 = new ExpressionNode("=", 1);
        ExpressionNode node3 = new ExpressionNode(">=", 2);
        expectedRootNode.setLeftChild(node2);
        expectedRootNode.setRightChild(node3);
        ExpressionNode node4 = new ExpressionNode("a", 3);
        ExpressionNode node5 = new ExpressionNode("2", 4);
        node2.setLeftChild(node4);
        node2.setRightChild(node5);
        ExpressionNode node6 = new ExpressionNode("b", 5);
        ExpressionNode node7 = new ExpressionNode("-3", 6);
        node3.setLeftChild(node6);
        node3.setRightChild(node7);
        Equation equation = new Equation("(a = 2) or (b >= -3)");
        assert treeEquals(expectedRootNode, equation.getMainNode());
    }

    private boolean treeEquals(ExpressionNode root1, ExpressionNode root2) {
        if (root1 == root2) {
            return true;
        }
        if (root1 == null || root2 == null) {
            return false;
        }
        return root1.getValue().equals(root2.getValue()) &&
                treeEquals(root1.getLeftChild(), root2.getLeftChild()) &&
                treeEquals(root1.getRightChild(), root2.getRightChild());
    }
}
