package pl.tesseract.atende.model;

import org.junit.Test;

import java.util.Iterator;

/**
 * @author Paweł Mielcarek
 */
public class RecordTest {

    @Test
    public void checkDefaultBounds() {
        assert Record.DEFAULT_MIN_VALUE < Record.DEFAULT_MAX_VALUE;
    }

    /**
     * Test generuje 1000 razy więcej rekordów niż różnica między DEFAULT_MAX i MIN,
     * następnie sprawdza czy wszystkie rekordy znajdują się w podanych granicach.
     */
    @Test
    public void createRandomRecordBoundsTest() {
        for (int i = 0; i < (Record.DEFAULT_MAX_VALUE - Record.DEFAULT_MIN_VALUE) * 1000; i++) {
            Record record = Record.createRandomRecord();
            record.getFields().values().forEach((v) -> {assert (v<=Record.DEFAULT_MAX_VALUE && v>=Record.DEFAULT_MIN_VALUE);});
        }
    }

    /**
     * Test sprawdza czy klucze rekordu to wartości od a do h.
     */
    @Test
    public void createRandomRecordKeysTest() {
        Record record = Record.createRandomRecord();
        assert record.getFields().size() == 8;
        Iterator<Character> keyIter = record.getFields().keySet().iterator();
        assert keyIter.next() == 'a';
        assert keyIter.next() == 'b';
        assert keyIter.next() == 'c';
        assert keyIter.next() == 'd';
        assert keyIter.next() == 'e';
        assert keyIter.next() == 'f';
        assert keyIter.next() == 'g';
        assert keyIter.next() == 'h';
    }

    /**
     * Test sprawdza czy metoda rzuca błąd przy podaniu wartości klucza innego niż a-h.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createIllegalArgumentTest() {
        Record.createRandomRecord().getField('z');
    }
}
