package pl.tesseract.atende.model.util;

import org.junit.Test;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Paweł Mielcarek
 */
public class OnpUtilTest {

    @Test
    public void mapSimpleExpressionTest() {
        List<String> simpleExpression = Arrays.asList("(", "a", "=", "2", ")", "or", "(", "b", ">=", "-3", ")");
        List<String> expectedOnpExpression = Arrays.asList("a", "2", "=", "b", "-3", ">=", "or");
        assertEquals(expectedOnpExpression, OnpUtil.mapSimpleExpression(simpleExpression));

        simpleExpression = Arrays.asList("(", "a", "=", "2", "and", "b", "=", "1", ")", "or", "(", "a", "=", "1", "and", "b", "=", "2", ")");
        expectedOnpExpression = Arrays.asList("a", "2", "=", "b", "1", "=", "and","a", "1", "=", "b", "2", "=", "and", "or");
        assertEquals(expectedOnpExpression, OnpUtil.mapSimpleExpression(simpleExpression));
    }
}
