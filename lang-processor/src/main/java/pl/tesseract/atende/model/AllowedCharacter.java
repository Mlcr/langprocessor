package pl.tesseract.atende.model;

/**
 * @author Paweł Mielcarek
 */
public enum AllowedCharacter {

    OR("or", 1, true, false),
    AND("and", 1, true, false),
    NOT_EQUALS("!=", 2, true, false),
    GREATER_EQUALS(">=", 2, true, false),
    GREATER(">", 2, true, false),
    LESS_EQUALS("<=", 2, true, false),
    LESS("<", 2, true, false),
    LEFT_BRACKET("(", 3, false, false),
    RIGHT_BRACKET(")", 3, false, false),
    EQUALS("=", 2, true, false),
    A("a", -1, false, true),
    B("b", -1, false, true),
    C("c", -1, false, true),
    D("d", -1, false, true),
    E("e", -1, false, true),
    F("f", -1, false, true),
    G("g", -1, false, true),
    H("h", -1, false, true);

    AllowedCharacter(String symbol, int importance, boolean operand, boolean letter) {
        this.symbol = symbol;
        this.importance = importance;
        this.operand = operand;
        this.letter = letter;
    }

    private String symbol;

    /**
     * Waga operatora - im wyższa tym bardziej operator wiąże.
     */
    private int importance;

    private boolean operand;
    private boolean letter;

    public static AllowedCharacter findBySymbol(String symbol) {
        AllowedCharacter allowedCharacterFound = null;
        for (AllowedCharacter allowedCharacter : values())
            if (allowedCharacter.symbol.equals(symbol))
                allowedCharacterFound = allowedCharacter;
        return allowedCharacterFound;
    }

    public static boolean isAllowedCharacter(String fragment) {
        return findBySymbol(fragment) != null || isInteger(fragment);
    }

    public static boolean isOperand(String fragment) {
        AllowedCharacter allowedCharacter = findBySymbol(fragment);
        return allowedCharacter != null && allowedCharacter.operand;
    }

    public static boolean isBracket(String symbol) {
        return symbol.equals(LEFT_BRACKET.getSymbol()) || symbol.equals(RIGHT_BRACKET.getSymbol());
    }

    public static boolean isLetter(String fragment) {
        AllowedCharacter allowedCharacter = findBySymbol(fragment);
        return allowedCharacter != null && allowedCharacter.letter;
    }

    public static boolean isInteger(String s) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),10) < 0) return false;
        }
        return true;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getImportance() {
        return importance;
    }
}
