package pl.tesseract.atende.model.util;

import pl.tesseract.atende.model.AllowedCharacter;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author Paweł Mielcarek
 */
public class OnpUtil {

    public static List<String> mapSimpleExpression(List<String> simpleExpression) {
        List<String> mappedExpression = new ArrayList<>();
        Stack<String> operandsStack = new Stack<>();
        for (String expressionPart : simpleExpression) {
            if (AllowedCharacter.LEFT_BRACKET.getSymbol().equals(expressionPart))
                operandsStack.push(expressionPart);
            else if (AllowedCharacter.RIGHT_BRACKET.getSymbol().equals(expressionPart)) {
                String operandFromStack = operandsStack.pop();
                while (!operandFromStack.equals(AllowedCharacter.LEFT_BRACKET.getSymbol())) {
                    mappedExpression.add(operandFromStack);
                    operandFromStack = operandsStack.pop();
                }
            } else if (!AllowedCharacter.isOperand(expressionPart))
                mappedExpression.add(expressionPart);
            else {
                while (!operandsStack.empty() && isOperandALessImportant(expressionPart, operandsStack.peek()) && !AllowedCharacter.isBracket(operandsStack.peek()))
                    mappedExpression.add(operandsStack.pop());
                operandsStack.push(expressionPart);
            }
        }
        mappedExpression.addAll(operandsStack);
        return mappedExpression;
    }

    private static boolean isOperandALessImportant(String operandA, String operandB) {
        return AllowedCharacter.findBySymbol(operandA).getImportance() <= AllowedCharacter.findBySymbol(operandB).getImportance();
    }
}
