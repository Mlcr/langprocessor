package pl.tesseract.atende.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author Paweł Mielcarek
 * Klasa reprezentująca jeden rekord posiadający 8 pól oznaczonych literami od a do h
 */
public class Record {

    public final static int DEFAULT_MIN_VALUE = -100;
    public final static int DEFAULT_MAX_VALUE = 100;

    public final static int MIN_KEY_ASCII = 97;
    public final static int MAX_KEY_ASCII = 104;

    private Map<Character, Integer> fields;

    private Record(Map<Character, Integer> fields) {
        this.fields = fields;
    }

    /**
     * Metoda inicjalizująca rekord losowymi danymi.
     * @return Rekord wypełniony losowymi danymi
     */
    public static Record createRandomRecord() {
        Random generator = new Random();
        Map<Character, Integer> fields = new HashMap<>();
        for (int i=0; i <= MAX_KEY_ASCII - MIN_KEY_ASCII; i++) {
            int random = generator.ints(DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE).findFirst().getAsInt();
            fields.put((char) (i + MIN_KEY_ASCII), random);
        }
        return new Record(fields);
    }

    public static Record createSpecificRecord() {
        Random generator = new Random();
        Map<Character, Integer> fields = new HashMap<>();
        fields.put('a', 1);
        fields.put('b', 2);
        for (int i=2; i <= MAX_KEY_ASCII - MIN_KEY_ASCII; i++) {
            int random = generator.ints(DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE).findFirst().getAsInt();
            fields.put((char) (i + MIN_KEY_ASCII), random);
        }
        return new Record(fields);
    }

    public int getField(char field) {
        if (field < MIN_KEY_ASCII || field > MAX_KEY_ASCII) throw new IllegalArgumentException();
        return fields.get(field);
    }

    public Map<Character, Integer> getFields() {
        return fields;
    }
}
