package pl.tesseract.atende.model;

/**
 * @author Paweł Mielcarek
 */
public class ExpressionNode {

    private String value;
    private int id;
    private ExpressionNode parent = null;
    private ExpressionNode leftChild = null;
    private ExpressionNode rightChild = null;

    public ExpressionNode(String value, int id) {
        this.value = value;
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ExpressionNode getParent() {
        return parent;
    }

    public void setParent(ExpressionNode parent) {
        this.parent = parent;
    }

    public ExpressionNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(ExpressionNode leftChild) {
        leftChild.setParent(this);
        this.leftChild = leftChild;
    }

    public ExpressionNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(ExpressionNode rightChild) {
        rightChild.setParent(this);
        this.rightChild = rightChild;
    }

    public int getId() {
        return id;
    }
}
