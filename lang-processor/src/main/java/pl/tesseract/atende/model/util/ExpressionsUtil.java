package pl.tesseract.atende.model.util;

import pl.tesseract.atende.model.AllowedCharacter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Paweł Mielcarek
 *
 * Util mapujący wyrażenia
 */
public class ExpressionsUtil {

    /**
     * Metoda mapująca dane równanie do onp.
     *
     * @param allowedCharacterList łańcuch znaków wyrażenia, rozbity na poszczególne fragmenty.
     * @return lista onp znaków równania
     */
    public static List<String> mapExpression(List<String> allowedCharacterList) {
        return OnpUtil.mapSimpleExpression(allowedCharacterList);
    }

    /**
     * Metoda usuwająca białe znaki z równania i mapująca je na listę charów.
     *
     * @param expression wyrażenie w postaci łańcucha znaków.
     * @return Lista symboli wyrażenia.
     */
    public static List<String> createSimpleExpression(String expression) {
        expression = expression.replaceAll("\\s+", "");
        return createAllowedCharactersList(expression);
    }

    /**
     * Metoda wyszukuje wszystkie symbole wyrażenia.
     *
     * @param expression wyrażenie
     * @return lista operandów
     */
    public static List<String> createAllowedCharactersList(String expression) throws IllegalArgumentException {
        List<String> allowedChars = new ArrayList<>();
        while (expression.length() > 0) {
            String longestAllowed = findLongestAllowedCharacter(expression);
            allowedChars.add(longestAllowed);
            expression = expression.substring(longestAllowed.length());
        }
        return allowedChars;
    }

    private static String findLongestAllowedCharacter(String expression) throws IllegalArgumentException{
        StringBuilder chainString = new StringBuilder();
        List<String> allowedStrings = new ArrayList<>();
        for (int i = 0; i < expression.length(); i++) {
            chainString.append(expression.charAt(i));
            if (AllowedCharacter.isAllowedCharacter(chainString.toString())) allowedStrings.add(chainString.toString());
        }
        if (allowedStrings.isEmpty()) throw new IllegalArgumentException();
        return allowedStrings.get(allowedStrings.size() - 1);
    }
}
