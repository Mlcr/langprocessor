package pl.tesseract.atende.model;

import pl.tesseract.atende.model.util.ExpressionsUtil;

import java.util.List;
import java.util.Stack;

/**
 * @author Paweł Mielcarek
 */
public class Equation {

    /**
     * Lista symboli wystepującyh w równaniu zmapowana do standardu onp.
     */
    private List<String> onpList;

    private ExpressionNode mainNode;

    public Equation(String expression) {
        List<String> allowedCharactersList = ExpressionsUtil.createSimpleExpression(expression);
        onpList = ExpressionsUtil.mapExpression(allowedCharactersList);
        mainNode = createTreeNodes();
    }

    private ExpressionNode createTreeNodes() {
        Stack<ExpressionNode> nodesStack = new Stack<>();
        int id = 0;
        for (String element : onpList) {
            ExpressionNode newNode = new ExpressionNode(element, id++);
            if (AllowedCharacter.isOperand(element)) {
                if (!nodesStack.empty())
                    newNode.setRightChild(nodesStack.pop());
                if (!nodesStack.empty())
                    newNode.setLeftChild(nodesStack.pop());
            }
            nodesStack.push(newNode);
        }
        return nodesStack.peek();
    }

    public ExpressionNode getMainNode() {
        return mainNode;
    }

    public boolean processRecord(Record record) {
        Stack<String> helperStack = new Stack<>();
        onpList.forEach(s -> {
            if (!AllowedCharacter.isOperand(s)) helperStack.push(s);
            else {
                AllowedCharacter allowedCharacter = AllowedCharacter.findBySymbol(s);
                if (allowedCharacter != AllowedCharacter.AND && allowedCharacter != AllowedCharacter.OR) {
                    String second = helperStack.pop();
                    Integer secondValue = getSymbolValue(second, record);
                    String first = helperStack.pop();
                    Integer firstValue = getSymbolValue(first, record);
                    if (allowedCharacter == AllowedCharacter.EQUALS) helperStack.push(String.valueOf(firstValue.equals(secondValue)));
                    if (allowedCharacter == AllowedCharacter.NOT_EQUALS) helperStack.push(String.valueOf(!firstValue.equals(secondValue)));
                    else if (allowedCharacter == AllowedCharacter.GREATER) helperStack.push(String.valueOf(firstValue > secondValue));
                    else if (allowedCharacter == AllowedCharacter.GREATER_EQUALS) helperStack.push(String.valueOf(firstValue >= secondValue));
                    else if (allowedCharacter == AllowedCharacter.LESS) helperStack.push(String.valueOf(firstValue < secondValue));
                    else if (allowedCharacter == AllowedCharacter.LESS_EQUALS) helperStack.push(String.valueOf(firstValue <= secondValue));
                } else {
                    boolean firstValue = getBooleanValue(helperStack.pop());
                    boolean secondValue = getBooleanValue(helperStack.pop());
                    if (allowedCharacter == AllowedCharacter.OR) helperStack.push(String.valueOf(firstValue || secondValue));
                    if (allowedCharacter == AllowedCharacter.AND) helperStack.push(String.valueOf(firstValue && secondValue));
                }
            }
        });
        return getBooleanValue(helperStack.pop());
    }

    private int getSymbolValue(String symbol, Record record) {
        if (AllowedCharacter.isLetter(symbol))
            return record.getField(symbol.charAt(0));
        else
            return Integer.valueOf(symbol);
    }

    private boolean getBooleanValue(String value) {
        return value.equals("true");
    }
}
