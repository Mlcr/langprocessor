package pl.tesseract.atende;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import pl.tesseract.atende.model.Equation;
import pl.tesseract.atende.view.LangTreeView;
import pl.tesseract.atende.view.RecordsView;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class AtendeUI extends UI {

    private LangTreeView langTreeView = null;

    private RecordsView recordsView;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();

        recordsView = new RecordsView();
        
        final TextField equation = new TextField();
        equation.setCaption("Wpisz równanie:");
        equation.setValue("(a = 1 and b = 2) or (a = 2 and b = 1)");

        Button button = createProcessButton(layout, equation);
        layout.addComponents(recordsView, equation, button);
        
        setContent(layout);
    }

    private Button createProcessButton(VerticalLayout layout, TextField equationField) {
        Button button = new Button("Procesuj");
        button.addClickListener( e -> {
            Equation equation;
            try {
                equation = new Equation(equationField.getValue());
                equationField.setComponentError(null);
                removeLangTreeView(layout);
                langTreeView = new LangTreeView(equation);
                layout.addComponent(langTreeView);
                recordsView.repaint(langTreeView.getEquation());
            } catch (IllegalArgumentException ex) {
                removeLangTreeView(layout);
                equationField.setComponentError(new ErrorMessage() {
                    @Override
                    public ErrorLevel getErrorLevel() {
                        return ErrorLevel.CRITICAL;
                    }

                    @Override
                    public String getFormattedHtmlMessage() {
                        return "Niepoprawne równanie";
                    }
                });
            }
        });
        return button;
    }

    private void removeLangTreeView(VerticalLayout layout) {
        if (langTreeView != null) {
            layout.removeComponent(langTreeView);
        }
    }

    @WebServlet(urlPatterns = "/*", name = "AtendeUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = AtendeUI.class, productionMode = false)
    public static class AtendeUIServlet extends VaadinServlet {
    }
}
