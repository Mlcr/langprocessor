package pl.tesseract.atende.view;

import com.vaadin.ui.TreeTable;
import pl.tesseract.atende.model.Equation;
import pl.tesseract.atende.model.ExpressionNode;

/**
 * @author Paweł Mielcarek
 */
public class LangTreeView extends TreeTable {

    private Equation equation;

    public LangTreeView(Equation equation) {
        this.equation = equation;
        init();
    }

    private void init() {
        addContainerProperty("Symbol", String.class, "");
        traverseTreeWithItems(equation.getMainNode());
        traverseTreeWithParents(equation.getMainNode());
        setWidth(500, Unit.PIXELS);
    }

    private void traverseTreeWithItems(ExpressionNode node) {
        if (node.getLeftChild() != null)
            traverseTreeWithItems(node.getLeftChild());
        if (node.getRightChild() != null)
            traverseTreeWithItems(node.getRightChild());
        addItem(new Object[]{node.getValue()}, node.getId());
    }

    private void traverseTreeWithParents(ExpressionNode node) {
        if (node.getLeftChild() != null)
            traverseTreeWithParents(node.getLeftChild());
        if (node.getRightChild() != null)
            traverseTreeWithParents(node.getRightChild());
        if (node.getParent()!=null)
            setParent(node.getId(), node.getParent().getId());
    }

    public Equation getEquation() {
        return equation;
    }
}
