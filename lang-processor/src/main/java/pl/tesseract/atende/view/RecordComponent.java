package pl.tesseract.atende.view;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import pl.tesseract.atende.model.Record;

/**
 * @author Paweł Mielcarek
 */
public class RecordComponent extends HorizontalLayout {

    private Record record;
    private int recordNo;
    boolean colored = false;

    public RecordComponent(Record record, int recordNo) {
        this.record = record;
        this.recordNo = recordNo;
        init();
    }

    public RecordComponent(Record record, int recordNo, boolean colored) {
        this.record = record;
        this.recordNo = recordNo;
        this.colored = colored;
        init();
    }

    private void init() {
        if (colored)
            addComponent(new Label(
                    String.format("<font color=\"red\"> Rekord nr: " + recordNo + " | " )
                    , ContentMode.HTML));
        else
            addComponent(new Label("Rekord nr: " + recordNo + " | "));
        record.getFields().entrySet().forEach(entry -> addComponent(new Label(entry.getKey() + "=" + entry.getValue() + " | ")));
    }
}
