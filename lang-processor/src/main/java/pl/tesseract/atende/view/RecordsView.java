package pl.tesseract.atende.view;

import com.vaadin.ui.VerticalLayout;
import pl.tesseract.atende.model.Equation;
import pl.tesseract.atende.model.Record;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Paweł Mielcarek
 */
public class RecordsView extends VerticalLayout {

    private List<Record> records = new ArrayList<>();

    public RecordsView() {
        for (int i = 0; i < 5; i++) {
            Record record = Record.createRandomRecord();
            records.add(record);
            addComponent(new RecordComponent(record, i + 1));
        }
        Record record = Record.createSpecificRecord();
        records.add(record);
        addComponent(new RecordComponent(record, 6));
    }

    public void repaint(Equation equation) {
        removeAllComponents();
        for (int i = 0; i < 6; i++) {
            Record record = records.get(i);
            addComponent(new RecordComponent(record, i + 1, equation.processRecord(record)));
        }
    }
}
